package com.digisafari.sapl.courseservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.digisafari.sapl.courseservice.model.Course;

@Repository
public interface CourseRepository extends MongoRepository<Course, String> {
	
	/*
	 * Repository layer is responsible for database opertations
	 * conneting to db, perform db opertation, CRUD methods
	 */

}
