package com.digisafari.sapl.courseservice.service;

import java.util.List;

import com.digisafari.sapl.courseservice.exceptions.CourseAlreadyExistsException;
import com.digisafari.sapl.courseservice.exceptions.CourseNotFoundException;
import com.digisafari.sapl.courseservice.model.Course;

public interface ICourseService {
	
	// business method
	public Course addCourse(Course course) throws CourseAlreadyExistsException;
	public List<Course> getAllCourses();
	public Course getCourseById(String id) throws CourseNotFoundException;
	public Course updateCourse(Course course) throws CourseNotFoundException;
	public boolean deleteCourse(String id) throws CourseNotFoundException;

}
