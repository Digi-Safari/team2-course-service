package com.digisafari.sapl.courseservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.digisafari.sapl.courseservice.exceptions.CourseAlreadyExistsException;
import com.digisafari.sapl.courseservice.model.Course;
import com.digisafari.sapl.courseservice.service.ICourseService;

@RestController //It declares the controller as REST controller which takes the incoming requests
@RequestMapping("api/v1")
public class CourseController {
	
	ResponseEntity<?> responseEntity;
	
	@Autowired
	ICourseService courseService;
	
	
	@PostMapping("/course")
	public ResponseEntity<?> addCourse(@RequestBody Course course) throws CourseAlreadyExistsException{
		Course createdCourse = null;
		/*
		 * 1. give this course object to CourseService for adding to the database
		 */
		try {
			createdCourse = courseService.addCourse(course);
			if(createdCourse != null)
			responseEntity = new ResponseEntity<>(createdCourse, HttpStatus.CREATED);
			else
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(CourseAlreadyExistsException courseAlreadyExistsException) {
			throw courseAlreadyExistsException;
		} catch(Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/course")
	public ResponseEntity<?> getAllCourses(){
		try {
			List<Course> coursesList = courseService.getAllCourses();
			responseEntity = new ResponseEntity<>(coursesList, HttpStatus.OK);
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return responseEntity;
	}

}
